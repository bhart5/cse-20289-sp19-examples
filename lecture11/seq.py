#!/usr/bin/env python3

import sys

limit = int(sys.argv[1])        # Try with 10000000

# Imperative
index = 0
while index < limit:
    print(index)
    index += 1

# List
numbers = list(range(limit))    # Materialize list
for index in numbers:
    print(index)

# Iterator
numbers = range(limit)          # Use iterator
for index in numbers:
    print(index)
