/* pipe.c: Manual popen */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <unistd.h>

int main(int argc, char *argv[]) {
    int status = EXIT_SUCCESS;
    int pfds[2];

    if (pipe(pfds) < 0) {
	fprintf(stderr, "Unable to pipe: %s\n", strerror(errno));
	return EXIT_FAILURE;
    }

    pid_t  pid = fork();
    if (pid < 0) {	    // Error
	fprintf(stderr, "Unable to fork: %s\n", strerror(errno));
	return EXIT_FAILURE;
    }

    if (pid == 0) {	    // Child
    	close(pfds[0]);			// Close unused read-end
    	dup2(pfds[1], STDOUT_FILENO);	// Bind stdout to write-end
    	close(pfds[1]);			// Close unused write-end

	if (execlp("ls", "ls", "-l", NULL) < 0) {
	    fprintf(stderr, "Unable to execlp: %s\n", strerror(errno));
	    _exit(EXIT_FAILURE);
	}
    } else {		    // Parent
    	close(pfds[1]);			// Close unused write-end

    	FILE *stream = fdopen(pfds[0], "r");
    	if (!stream) {
	    fprintf(stderr, "Unable to fdopen: %s\n", strerror(errno));
    	    return EXIT_FAILURE;
	}

	char buffer[BUFSIZ];
	while (fgets(buffer, BUFSIZ, stream)) {
	    fputs(buffer, stdout);
	}
    }

    return status;
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
