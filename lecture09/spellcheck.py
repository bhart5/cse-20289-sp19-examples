#!/usr/bin/env python3

import sys

# Constants

PATH  = '/usr/share/dict/words'

# Functions

def load_words_list(path=PATH):
    words = []
    for word in open(path):
        words.append(word.strip())
    return words

def load_words_dict(path=PATH):
    words = {}
    for word in open(path):
        words[word.strip()] = True
    return words

def load_words_set(path=PATH):
    words = set()
    for word in open(path):
        words.add(word.strip())
    return words

# Main Execution

if __name__ == '__main__':
    #words = load_words_list()
    words = load_words_dict()
    #words = load_words_set()

    for line in sys.stdin:
        for word in line.strip().split():
            if word not in words:
                print('{} is misspelled!'.format(word))
