#!/usr/bin/env python3

import random
import os
import re
import sys

# Functions

def usage(status=0):
    print('''Usage: {} [options] SOURCE TARGET TOP BOTTOM
    -c FILL     Fill color for text (default: white)
    -f FONT     Font for text (default: Helvetica-Bold)
    -s SCALE    How large to scale font (default: 0.1)
    -d          Deep fry it
'''.format(os.path.basename(sys.argv[0])))
    sys.exit(status)

def get_image_dimensions(path):
    result  = os.popen('identify {}'.format(path)).read()
    matches = re.findall('([0-9]+)x([0-9]+)', result)
    return matches[0]

def generate_meme(source, target, top, bottom, font='Helvetica-Bold', scale=0.1, fill='white', fried=False):
    width, height = get_image_dimensions(source)
    pointsize     = int(int(width) * scale)
    top_text      = [
        '-font', font,
        '-fill', fill,
        '-pointsize', str(pointsize),
        '-stroke', 'black',
        '-strokewidth', '2',
        '-gravity', 'north',
        '-annotate', '0',
        '"{}"'.format(top),
    ]
    bottom_text   = [
        '-font', font,
        '-fill', fill,
        '-pointsize', str(pointsize),
        '-stroke', 'black',
        '-strokewidth', '2',
        '-gravity', 'south',
        '-annotate', '0',
        '"{}"'.format(bottom),
    ]
    if fried:
        transforms = [
            '-emboss', '0x2', 
            '-swirl', str(random.randint(-60,60)),
            '+noise', 'Impulse',
        ]
    else:
        transforms = []
    command       = ['convert', source] + top_text + bottom_text + transforms + [target]
    os.system(' '.join(command))

# Parse Command-line Options

font  = 'Helvetica-Bold'
scale = 0.1
fill  = 'white'
fried = False
args  = sys.argv[1:]

while args and args[0].startswith('-') and len(args[0]) > 1:
    arg = args.pop(0)
    if arg == '-f':
        font = args.pop(0)
    elif arg == '-c':
        fill = args.pop(0)
    elif arg == '-s':
        scale = float(args.pop(0))
    elif arg == '-d':
        fried = True
    else:
        usage(1)

if len(args) != 4:
    usage(1)

source, target, top, bottom = args

# Main Execution

generate_meme(source, target, top, bottom, font, scale, fill, fried)
