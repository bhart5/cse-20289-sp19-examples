#!/usr/bin/env python3

# Variant 1: check exit status of os.system

import os
import random
import sys

BLACKLIST  = {'bong', 'sodomize', 'kiss', 'head-in', 'telebears'}

CHARACTERS = []
for index, line in enumerate(os.popen('cowsay -l')):
    if not index:
        continue
    for character in line.split():
        if character not in BLACKLIST:
            CHARACTERS.append(character)

SELECTED = random.choice(CHARACTERS)
os.system('cowsay -f {}'.format(SELECTED))
