#!/usr/bin/env python3

import pprint
import requests
import sys

url    = 'https://en.wikipedia.org/w/api.php'
params = {'action': 'query', 'list': 'search', 'format': 'json', 'srsearch': sys.argv[1]}
result = requests.get(url, params=params)
data   = result.json()

#pprint.pprint(data)
#articles = sorted(data['query']['search'], key=lambda o: o['wordcount'])
#for index, entry in enumerate(articles, 1):

for index, entry in enumerate(data['query']['search'], 1):
    print('{:4}.\t{}'.format(index, entry['title']))

